# Install

Mobilizon can be installed through different methods:

- [install a (precompiled) release](./release.md) (recommended)  
  Supported systems:
    - Debian Bookworm,
    - Debian Bullseye,
    - Ubuntu Jammy,
    - Ubuntu Focal,
    - Ubuntu Bionic,
    - Fedora 38,
    - Fedora 39  

Other systems may work, but are not supported.

- [run a Docker image](./docker.md) (standalone or with `docker compose`)
- [install from source](./source.md)
- [Using the community-maintained module for NixOS](./nixos.md)
