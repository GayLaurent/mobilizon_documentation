# Logging

Mobilizon outputs to the standard output. If you use `systemd` as a service manager for Mobilizon, then the logs are probably accessible through `journald`.

```
sudo journalctl -u mobilizon -f
```

## Log-level & -formatting

The log level or format can be configured in the configuration file like this:

```elixir
config :logger, :console, format: "[$level] $message\n", level: :debug
```

Available common levels are `:debug`, :info` (default) `:warning`. [The full list is accessible here](https://hexdocs.pm/logger/Logger.html#module-levels).

## Sentry

### Backend
Mobilizon offers Sentry integration to track errors. Simply add the following configuration to start sending errors and stacktraces to Sentry. Restart Mobilizon afterwards.

```elixir
config :sentry,
    dsn: "https://public_key@app.getsentry.com/1", # Your Sentry DSN
    release: to_string(Application.spec(:mobilizon, :vsn)),
    server_name: "my-mobilizon-server.org" # Your instance domain name

config :logger, :console, backends: [:console, Sentry.LoggerBackend]

config :mobilizon, Mobilizon.Service.ErrorReporting,
    adapter: Mobilizon.Service.ErrorReporting.Sentry
```

### Frontend
If you want Sentry to be loaded on Front-end as well, you need to add as well:
```elixir
config :mobilizon, Mobilizon.Service.FrontEndAnalytics.Sentry,
  enabled: true,
  dsn: "https://public_key@app.getsentry.com/1", # Your Sentry DSN
  tracesSampleRate: 1.0,
  organization: "yourorganisation",
  project: "yourproject",
  host: "sentry.yourdomain.org", # Your self-hosted sentry hostname, otherwise sentry.io
  csp: [
    connect_src: ["sentry.yourdomain.org"]
  ]
```

As well as registering the Sentry analytics provider
```elixir
config :mobilizon, :analytics,
  providers: [
    Mobilizon.Service.FrontEndAnalytics.Sentry
  ]
```

The feedback feature will also be used if there's a crash on front-end.