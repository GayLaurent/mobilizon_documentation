# Manage event participants

## List participants

As the event creator you can see who particpates in an event. To do so you can click either:

  * the link **X persons going** on your event page:

      ![number of participants - event view](../../images/en/event-participations-number-event-page-EN.png)

  * or **My events** on top bar menu and the **Manage participations** link:

      ![event actions image](../../images/en/events-page-manage-participations-EN.png)

![event participants list](../../images/en/event-participations-list-messages-EN.png)

## Reject a participant

![gif reject participant actions](../../images/en/event-participations-list-messages-reject-EN.gif)

To reject participants head to [the participations list](#list-participations) and:

  1. check the participant boxes you want to reject
  * click the **Reject participant** button

Once done, the **Rejected** label will be displayed:

![image rejected particpant](../../images/en/event-participations-list-messages-rejected-EN.png)

!!! info
    The participant will receive a notification of this rejection by email.

## Approve participant

When **I want to approve every participation request** option is checked by the event organiser, participants see a modal where they can optionally add a short text and have to click the **Confirm my participation** button:

![participation confirmation modal](../../images/en/event-participation-confirmation.png)

The organiser can approve participation by [going to participations list](#list-participations) and:

  1. clicking checkboxes in front of participants
  * clicking **Approve participant**

![image participation approval](../../images/en/event-participation-approval.png)

Once done, the **Participant** label will be displayed:

![image label participant](../../images/en/event-participation-approved.png)
